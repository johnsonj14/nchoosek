public class ZZZ
{
   /**
    * recursive solution to solve n choose k
    *
    * @param n is the number of elements to choose from
    * @param k is the number of elements chosen
    * @return n choose k
    */
   public static long nChooseKRecursive(int n, int k)
   {
      if ((k == 0) || (n == k))
      {
         return 1;
      }
      else
      {
         return nChooseKRecursive(n - 1, k - 1) + nChooseKRecursive(n - 1, k);
      }
   }
   

   /**
    * dynamic solution to solve n choose k
    *
    * @param n is the number of elements to choose from
    * @param k is the number of elements chosen
    * @return n choose k
    */
   public static long nChooseKDynamic(int n, int k)
   {
      long dynamic[] = new long[k + 1];
      dynamic[0] = 1;

      // for loop that uses pascal triangle
      for (int i = 1; i <= n; i++)
      {
         // Compute next row of pascal triangle using
         // the previous row
         for (int j = Math.min(i, k); j > 0; j--)
         {
            dynamic[j] = dynamic[j] + dynamic[j - 1];
         }
      }
      return dynamic[k];
   }
   

   /**
    * equation to solve n choose k
    *
    * @param n is the number of elements to choose from
    * @param k is the number of elements chosen
    * @return n choose k
    */
   public static long nChooseK(int n, int k)
   {
      long result = 1;

      if (k > n - k)
      {
         k = n - k;
      }

      for (int i = 1, m = n; i <= k; i++, m--)
      {

         result = result * m / i;
      }
      return result;
   }
   

   /**
    * computes the factorial of a certain number starting at 1
    *
    * @param n is the number
    * @return the factorial of n
    */
   private static long factorial(int n)
   {
      long factorial = 1;
      for (int i = 1; i <= n; i++)
      {
         factorial *= i;
      }
      return factorial;
   }
}